
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h> 
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <random>
#include <vector>
#include "CStopWatch.h"

std::random_device rd;                         // only used once to initialise (seed) engine
std::mt19937 rng(rd());                        // random-number engine used (Mersenne-Twister in this case)
std::uniform_int_distribution<int> uni(0, 20); // guaranteed unbiased

void print(std::vector < double > v){
    for(int i=0; i<v.size(); i++){
        std::cout << v[i] << " ";
    }
}
void print(std::vector < std::vector < double > > m){
    for(int i=0; i<m.size(); i++){
        for(int j=0; j<m[i].size(); j++){
            std::cout << std::setw(5) << m[i][j] << " ";
        }
        std::cout << "\n";
    }
}
std::vector < std::vector < double > > genMatrix(int m, int n){

    std::vector < std::vector < double > > retValue;

    for(int i=0; i<m; i++){
        retValue.push_back(std::vector < double >(n));
    }

    for(int i=0; i<m; i++){
        for(int j=0; j<n; j++){
            retValue[i][j] = uni(rng);
        }
    }

    return retValue;
}
std::vector < double >genVector(int n){

    std::vector < double > retValue(n); 
    for(int i=0; i<n; i++){
        retValue[i] = uni(rng);
    }

    return retValue;
}

std::vector < double > matVecMult(std::vector < std::vector < double > > A, std::vector < double > x){

    std::vector < double > retValue(A.size());
    for(int i=0; i<A.size(); i++){
        retValue[i] = 0;
        for(int j=0; j<x.size(); j++){
            retValue[i] += A[i][j] * x[j];
        }
    }

    return retValue;
}

int main() {

    std::vector < std::vector < double > > A;
    std::vector < double > x;
    std::vector < double > y;

    int m=10, n=10;
    A = genMatrix(m, n);
    x = genVector(n);
    y = matVecMult(A, x);


    std::cout << "A:\n";
    print(A);
    std::cout << "\n";

    std::cout << "x:\n";
    print(x);
    std::cout << "\n\n";

    std::cout << "y:\n";
    print(y);
    std::cout << "\n";
    
    return 0;
} 